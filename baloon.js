const balloon = function(text) {
  let word = 'balloon';
  let count = 0;
  const number = Math.floor(text.length / 7);
  for (let i = 0; i <= number; i++) {
    for (let char of text) {
      // console.log(char);
      // console.log(word);
      // console.log(count)
      if(word === '') {
        count += 1;
        word = 'balloon';
      } 
      if(word && word.includes(char)) {
        word = word.replace(char, '');
        text = text.replace(char, '');
      } 
    }
  }
  if(word === '') {
    count += 1;
  }
  return count;
}


let a = balloon('abcdealloonghijklmnopqrstuvwxyz');
let b = balloon('balloonballoonballoon');

console.log(a);
console.log(b);